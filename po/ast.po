# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Xfce
# This file is distributed under the same license as the libxfce4ui.master package.
# 
# Translators:
# enolp <enolp@softastur.org>, 2015-2016,2020,2024
# Ḷḷumex03, 2014
# Ḷḷumex03, 2014
msgid ""
msgstr ""
"Project-Id-Version: Libxfce4ui\n"
"Report-Msgid-Bugs-To: https://gitlab.xfce.org/\n"
"POT-Creation-Date: 2023-10-19 12:45+0200\n"
"PO-Revision-Date: 2013-07-02 20:33+0000\n"
"Last-Translator: enolp <enolp@softastur.org>, 2015-2016,2020,2024\n"
"Language-Team: Asturian (http://app.transifex.com/xfce/libxfce4ui/language/ast/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ast\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: libxfce4ui/xfce-dialogs.c:110
msgid "Failed to open web browser for online documentation"
msgstr "Fallu al abrir el restolador pa la documentación en llinia"

#: libxfce4ui/xfce-dialogs.c:265
#, c-format
msgid "Do you want to read the %s manual online?"
msgstr "¿Quies lleer el manual en llinia de «%s»?"

#: libxfce4ui/xfce-dialogs.c:267
msgid "Do you want to read the manual online?"
msgstr "¿Quies lleer el manual en llinia?"

#: libxfce4ui/xfce-dialogs.c:270
msgid "Read the manual"
msgstr ""

#: libxfce4ui/xfce-dialogs.c:273
msgid ""
"You will be redirected to the documentation website where the help pages are"
" maintained and translated."
msgstr "Redireicionarásete al sitiu web de documentación u tán calteníes y tornaes les páxines d'ayuda."

#. Create cancel button
#: libxfce4ui/xfce-dialogs.c:275 libxfce4ui/xfce-dialogs.c:533
#: libxfce4kbd-private/xfce-shortcut-dialog.c:254
msgid "_Cancel"
msgstr "_Encaboxar"

#: libxfce4ui/xfce-dialogs.c:277
msgid "_Read Online"
msgstr "Llee_r en llinia"

#: libxfce4ui/xfce-dialogs.c:286
msgid "_Always go directly to the online documentation"
msgstr "_Dir siempres a la documentación en llinia direutamente"

#: libxfce4ui/xfce-dialogs.c:335 libxfce4ui/xfce-dialogs.c:370
#: libxfce4ui/xfce-dialogs.c:406
msgid "Close"
msgstr "Zarrar"

#: libxfce4ui/xfce-dialogs.c:453
msgid "No"
msgstr "Non"

#: libxfce4ui/xfce-dialogs.c:455
msgid "Yes"
msgstr "Sí"

#: libxfce4ui/xfce-dialogs.c:459 libxfce4kbd-private/xfce-shortcuts-xfwm4.c:41
msgid "Cancel"
msgstr "Encaboxar"

#: libxfce4ui/xfce-dialogs.c:518
msgid "Close window with multiple tabs?"
msgstr ""

#: libxfce4ui/xfce-dialogs.c:520
msgid ""
"This window has multiple tabs open. Closing this window will also close all "
"its tabs."
msgstr ""

#: libxfce4ui/xfce-dialogs.c:523
#, c-format
msgid ""
"This window has %d tabs open. Closing this window will also close all its "
"tabs."
msgstr ""

#: libxfce4ui/xfce-dialogs.c:529
msgid "Warning"
msgstr "Atención"

#: libxfce4ui/xfce-dialogs.c:534
msgid "Close T_ab"
msgstr "Z_arrar la llingüeta"

#: libxfce4ui/xfce-dialogs.c:535
msgid "Close _Window"
msgstr "Zarrar la _ventana"

#: libxfce4ui/xfce-dialogs.c:540
msgid "Do _not ask me again"
msgstr "Nu_n volver entrugar"

#: libxfce4ui/xfce-sm-client.c:1427
msgid "Session management client ID"
msgstr "ID de veceru del alministrador de sesión"

#: libxfce4ui/xfce-sm-client.c:1427
msgid "ID"
msgstr "ID"

#: libxfce4ui/xfce-sm-client.c:1428
msgid "Disable session management"
msgstr "Deshabilitar alministración de sesión"

#: libxfce4ui/xfce-sm-client.c:1438
msgid "Session management options"
msgstr "Opciones d'alministración de sesión"

#: libxfce4ui/xfce-sm-client.c:1439
msgid "Show session management options"
msgstr "Amosar opciones d'alministración de sesión"

#: libxfce4ui/xfce-sm-client.c:1609
#, c-format
msgid "Failed to connect to the session manager: %s"
msgstr "Fallu al coneutar al xestor de sesión: %s"

#: libxfce4ui/xfce-sm-client.c:1615
msgid "Session manager did not return a valid client id"
msgstr "El xestor de sesión nun devolvió una ID de veceru válida"

#. print warning for user
#: libxfce4ui/xfce-spawn.c:397
#, c-format
msgid ""
"Working directory \"%s\" does not exist. It won't be used when spawning "
"\"%s\"."
msgstr "Nun esiste'l direutoriu de trabayu «%s». Nun s'usará cuando apruza \"%s\"."

#. retrieve the error and warning messages
#: libxfce4ui/xfce-filename-input.c:232
msgid "Filename is too long"
msgstr "El nome del ficheru ye mui llongu"

#: libxfce4ui/xfce-filename-input.c:233
msgid "Directory separator illegal in file name"
msgstr ""

#: libxfce4ui/xfce-filename-input.c:234
msgid "Filenames should not start or end with a space"
msgstr ""

#: libxfce4kbd-private/xfce-shortcuts.c:46
#, c-format
msgid ""
"This shortcut is already being used for the action '%s'. Which action do you"
" want to use?"
msgstr "Esti accesu direutu yá ta usándose pa la aición '%s\". ¿Qué aición quies usar?"

#: libxfce4kbd-private/xfce-shortcuts.c:47
#: libxfce4kbd-private/xfce-shortcuts.c:50
#: libxfce4kbd-private/xfce-shortcuts.c:53
#: libxfce4kbd-private/xfce-shortcuts.c:56
#, c-format
msgid "Use '%s'"
msgstr "Usar '%s'"

#: libxfce4kbd-private/xfce-shortcuts.c:47
#: libxfce4kbd-private/xfce-shortcuts.c:50
#: libxfce4kbd-private/xfce-shortcuts.c:53
#: libxfce4kbd-private/xfce-shortcuts.c:56
#, c-format
msgid "Keep '%s'"
msgstr "Caltener '%s'"

#: libxfce4kbd-private/xfce-shortcuts.c:49
#: libxfce4kbd-private/xfce-shortcuts.c:52
#, c-format
msgid ""
"This shortcut is already being used for the command '%s'. Which action do "
"you want to use?"
msgstr "Esti accesu direutu yá ta usándose pal comandu '%s\". ¿Qué aición quies usar?"

#: libxfce4kbd-private/xfce-shortcuts.c:55
#, c-format
msgid ""
"This shortcut is already being used by the action '%s'. Which action do you "
"want to use?"
msgstr "Esti accesu direutu yá ta usándose pola aición '%s\". ¿Qué aición quies usar?"

#. This shortcut already exists in the provider, we don't want it twice
#. Warn the user
#: libxfce4kbd-private/xfce-shortcuts.c:106
msgid "Please use another key combination."
msgstr "Usa otra combinación de tecles."

#: libxfce4kbd-private/xfce-shortcuts.c:107
#, c-format
msgid "%s already triggers this action."
msgstr "%s yá dispara esta aición."

#: libxfce4kbd-private/xfce-shortcuts.c:112
#, c-format
msgid "Conflicting actions for %s"
msgstr "Aiciones en conflictu pa %s"

#: libxfce4kbd-private/xfce-shortcuts.c:174
msgid "This shortcut is already being used for something else."
msgstr "Esti atayu yá s'usa pa daqué más."

#: libxfce4kbd-private/xfce-shortcuts-editor-dialog.c:94
#: libxfce4kbd-private/xfce-shortcuts-editor-dialog.c:131
msgid "Shortcuts Editor"
msgstr "Editor d'atayos"

#: libxfce4kbd-private/xfce-shortcuts-editor.c:380
msgid "The current shortcut. Click to edit..."
msgstr "L'atayu actual. Calca pa editalu…"

#: libxfce4kbd-private/xfce-shortcuts-editor.c:391
msgid "Clear the shortcut"
msgstr ""

#: libxfce4kbd-private/xfce-shortcuts-editor.c:401
msgid "Restore the default shortcut"
msgstr ""

#. skip leading slash
#: libxfce4kbd-private/xfce-shortcuts-editor.c:473
#: libxfce4kbd-private/xfce-shortcuts-editor.c:562
#, c-format
msgid "This keyboard shortcut is currently used by: '%s'"
msgstr ""

#: libxfce4kbd-private/xfce-shortcuts-editor.c:476
#: libxfce4kbd-private/xfce-shortcuts-editor.c:565
msgid "Keyboard shortcut already in use"
msgstr ""

#: libxfce4kbd-private/xfce-shortcut-dialog.c:217
msgid "Window Manager Action Shortcut"
msgstr "Accesu direutu d'aición del xestor de ventanes"

#. TRANSLATORS: this string will be used to create an explanation for
#. * the user in a following string
#: libxfce4kbd-private/xfce-shortcut-dialog.c:220
#: libxfce4kbd-private/xfce-shortcut-dialog.c:236
msgid "action"
msgstr "aición"

#: libxfce4kbd-private/xfce-shortcut-dialog.c:225
msgid "Command Shortcut"
msgstr "Accesu direutu de comandu"

#. TRANSLATORS: this string will be used to create an explanation for
#. * the user in a following string
#: libxfce4kbd-private/xfce-shortcut-dialog.c:228
msgid "command"
msgstr "comandu"

#: libxfce4kbd-private/xfce-shortcut-dialog.c:233
msgid "Shortcut"
msgstr "Atayu"

#: libxfce4kbd-private/xfce-shortcut-dialog.c:248
msgid "Clear"
msgstr "Llimpiar"

#: libxfce4kbd-private/xfce-shortcut-dialog.c:271
#, c-format
msgid "Press keyboard keys to trigger the %s '%s'."
msgstr ""

#: libxfce4kbd-private/xfce-shortcut-dialog.c:293
msgid "Please press a key"
msgstr "Primi una tecla"

#: libxfce4kbd-private/xfce-shortcut-dialog.c:348
msgid "Could not grab the keyboard."
msgstr "Nun pudo coyese'l tecláu."

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:36
msgid "Window operations menu"
msgstr "Menú d'operaciones de ventana"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:37
msgid "Up"
msgstr "Arriba"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:38
msgid "Down"
msgstr "Abaxo"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:39
msgid "Left"
msgstr "Esquierda"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:40
msgid "Right"
msgstr "Derecha"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:42
msgid "Cycle windows"
msgstr "Percorrer les ventanes"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:43
msgid "Cycle windows (Reverse)"
msgstr "Esbillar ventanes cíclicamente (al revés)"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:44
msgid "Switch window for same application"
msgstr "Cambiar ventana pa la mesma aplicación"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:45
msgid "Switch application"
msgstr "Cambiar d'aplicación"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:46
msgid "Close window"
msgstr "Zarrar ventana"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:47
msgid "Maximize window horizontally"
msgstr "Maximizar ventana horizontalmente"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:48
msgid "Maximize window vertically"
msgstr "Maximizar ventana verticalmente"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:49
msgid "Maximize window"
msgstr "Maximizar ventana"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:50
msgid "Hide window"
msgstr "Anubrir ventana"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:51
msgid "Move window"
msgstr "Mover ventana"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:52
msgid "Resize window"
msgstr "Redimensionar ventana"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:53
msgid "Shade window"
msgstr "Endolcar ventana"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:54
msgid "Stick window"
msgstr "Apegar ventana"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:55
msgid "Raise window"
msgstr "Elevar ventana"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:56
msgid "Lower window"
msgstr "Baxar ventana"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:57
msgid "Raise or lower window"
msgstr "Elevar o baxar ventana"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:58
msgid "Fill window"
msgstr "Ocupar tola ventana"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:59
msgid "Fill window horizontally"
msgstr "Ocupar la ventana horizontalmente"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:60
msgid "Fill window vertically"
msgstr "Ocupar la ventana verticalmente"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:61
msgid "Toggle above"
msgstr "Amosar enriba"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:62
msgid "Toggle fullscreen"
msgstr "Pantalla completa"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:63
msgid "Move window to lower monitor"
msgstr ""

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:64
msgid "Move window to left monitor"
msgstr ""

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:65
msgid "Move window to right monitor"
msgstr ""

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:66
msgid "Move window to upper monitor"
msgstr ""

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:67
msgid "Move window to upper workspace"
msgstr "Mover ventana a la estaya de trabayu superior"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:68
msgid "Move window to bottom workspace"
msgstr "Mover ventana a la estaya de trabayu inferior"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:69
msgid "Move window to left workspace"
msgstr "Mover ventana a la estaya de trabayu esquierda"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:70
msgid "Move window to right workspace"
msgstr "Mover ventana a la estaya de trabayu drecha"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:71
msgid "Move window to previous workspace"
msgstr "Mover ventana a la estaya de trabayu anterior"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:72
msgid "Move window to next workspace"
msgstr "Mover ventana a la estaya de trabayu siguiente"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:73
msgid "Move window to workspace 1"
msgstr "Mover ventana a la estaya de trabayu 1"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:74
msgid "Move window to workspace 2"
msgstr "Mover ventana a la estaya de trabayu 2"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:75
msgid "Move window to workspace 3"
msgstr "Mover ventana a la estaya de trabayu 3"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:76
msgid "Move window to workspace 4"
msgstr "Mover ventana a la estaya de trabayu 4"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:77
msgid "Move window to workspace 5"
msgstr "Mover ventana a la estaya de trabayu 5"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:78
msgid "Move window to workspace 6"
msgstr "Mover ventana a la estaya de trabayu 6"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:79
msgid "Move window to workspace 7"
msgstr "Mover ventana a la estaya de trabayu 7"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:80
msgid "Move window to workspace 8"
msgstr "Mover ventana a la estaya de trabayu 8"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:81
msgid "Move window to workspace 9"
msgstr "Mover ventana a la estaya de trabayu 9"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:82
msgid "Move window to workspace 10"
msgstr "Mover ventana a la estaya de trabayu 10"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:83
msgid "Move window to workspace 11"
msgstr "Mover ventana a la estaya de trabayu 11"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:84
msgid "Move window to workspace 12"
msgstr "Mover ventana a la estaya de trabayu 12"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:85
msgid "Tile window to the top"
msgstr "Mosaicu de ventanes hacia arriba"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:86
msgid "Tile window to the bottom"
msgstr "Mosaicu de ventanes hacia abajo"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:87
msgid "Tile window to the left"
msgstr "Mosaicu de ventanes a la izquierda"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:88
msgid "Tile window to the right"
msgstr "Mosaicu de ventanes a la drecha"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:89
msgid "Tile window to the top-left"
msgstr ""

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:90
msgid "Tile window to the top-right"
msgstr ""

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:91
msgid "Tile window to the bottom-left"
msgstr ""

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:92
msgid "Tile window to the bottom-right"
msgstr ""

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:93
msgid "Show desktop"
msgstr "Amosar escritoriu"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:94
msgid "Upper workspace"
msgstr "Estaya de trabayu superior"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:95
msgid "Bottom workspace"
msgstr "Estaya de trabayu inferior"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:96
msgid "Left workspace"
msgstr "Estaya de trabayu esquierda"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:97
msgid "Right workspace"
msgstr "Estaya de trabayu drecha"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:98
msgid "Previous workspace"
msgstr "Estaya de trabayu anterior"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:99
msgid "Next workspace"
msgstr "Estaya de trabayu siguiente"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:100
msgid "Workspace 1"
msgstr "Estaya de trabayu 1"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:101
msgid "Workspace 2"
msgstr "Estaya de trabayu 2"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:102
msgid "Workspace 3"
msgstr "Estaya de trabayu 3"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:103
msgid "Workspace 4"
msgstr "Estaya de trabayu 4"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:104
msgid "Workspace 5"
msgstr "Estaya de trabayu 5"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:105
msgid "Workspace 6"
msgstr "Estaya de trabayu 6"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:106
msgid "Workspace 7"
msgstr "Estaya de trabayu 7"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:107
msgid "Workspace 8"
msgstr "Estaya de trabayu 8"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:108
msgid "Workspace 9"
msgstr "Estaya de trabayu 9"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:109
msgid "Workspace 10"
msgstr "Estaya de trabayu 10"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:110
msgid "Workspace 11"
msgstr "Estaya de trabayu 11"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:111
msgid "Workspace 12"
msgstr "Estaya de trabayu 12"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:112
msgid "Add workspace"
msgstr "Amestar estaya de trabayu"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:113
msgid "Add adjacent workspace"
msgstr "Amestar estaya de trabayu cercana"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:114
msgid "Delete last workspace"
msgstr "Desaniciar la cabera estaya de trabayu"

#: libxfce4kbd-private/xfce-shortcuts-xfwm4.c:115
msgid "Delete active workspace"
msgstr "Desaniciar la estaya de trabayu activa"

#: xfce4-about/main.c:58
msgid "Version information"
msgstr "Información de la versión"

#: xfce4-about/main.c:154
msgid "GPUs"
msgstr "GPUs"

#: xfce4-about/main.c:172
msgid "Window Manager"
msgstr "Xestor de ventanes"

#: xfce4-about/main.c:173
msgid "Handles the placement of windows on the screen."
msgstr "Remana l'asitiamientu de ventanes na pantalla."

#: xfce4-about/main.c:177
msgid "Panel"
msgstr "Panel"

#: xfce4-about/main.c:178
msgid "Provides a home for window buttons, launchers, app menu and more."
msgstr ""

#: xfce4-about/main.c:182
msgid "Desktop Manager"
msgstr "Xestor d'escritoriu"

#: xfce4-about/main.c:183
msgid "Sets desktop backgrounds, handles icons and more."
msgstr ""

#: xfce4-about/main.c:187
msgid "File Manager"
msgstr "Xestor de ficheros"

#: xfce4-about/main.c:188
msgid "Manages your files in a modern, easy-to-use and fast way."
msgstr ""

#: xfce4-about/main.c:192
msgid "Volume Manager"
msgstr "Xestor de volúmenes"

#: xfce4-about/main.c:193
msgid "Manages removable drives and media for Thunar."
msgstr ""

#: xfce4-about/main.c:197
msgid "Session Manager"
msgstr "Xestor de sesión"

#: xfce4-about/main.c:198
msgid ""
"Saves and restores your session, handles startup, autostart and shutdown."
msgstr ""

#: xfce4-about/main.c:202
msgid "Settings Manager"
msgstr "Xestor d'axustes"

#: xfce4-about/main.c:203
msgid "Configures appearance, display, keyboard and mouse settings."
msgstr ""

#: xfce4-about/main.c:207
msgid "Application Finder"
msgstr "Guetador d'aplicaciones"

#: xfce4-about/main.c:208
msgid "Quickly finds and launches applications installed on your system."
msgstr ""

#: xfce4-about/main.c:212
msgid "Settings Daemon"
msgstr "Degorriu d'axustes"

#: xfce4-about/main.c:213
msgid "Stores your settings in a D-Bus-based configuration system."
msgstr ""

#: xfce4-about/main.c:217
msgid "A Menu Library"
msgstr ""

#: xfce4-about/main.c:218
msgid "Implements a freedesktop.org compliant menu based on GLib and GIO."
msgstr ""

#: xfce4-about/main.c:222
msgid "Thumbnails Service"
msgstr "Serviciu de miniatures"

#: xfce4-about/main.c:223
msgid "Implements the thumbnail management D-Bus specification."
msgstr ""

#: xfce4-about/main.c:277
msgid "Please see <https://www.xfce.org/about/credits>"
msgstr ""

#: xfce4-about/main.c:338
msgid ""
"If you know of anyone missing from this list; don't hesitate and file a bug "
"on <https://gitlab.xfce.org/xfce/libxfce4ui/-/issues> ."
msgstr ""

#: xfce4-about/main.c:342
msgid "Thanks to all who helped making this software available!"
msgstr "¡Gracies a toles persones qu'ayudaron a facer esti software disponible!"

#: xfce4-about/main.c:359
msgid ""
"Xfce 4 is copyright Olivier Fourdan (fourdan@xfce.org). The different "
"components are copyrighted by their respective authors."
msgstr "Xfce 4 tien copyright d'Oliver Fourdan (fourdan@xfce.org). Los distintos componentes tienen copyright de los sos respeutivos autores."

#: xfce4-about/main.c:364
msgid ""
"The libxfce4ui, libxfce4util and exo packages are distributed under the "
"terms of the GNU Library General Public License as published by the Free "
"Software Foundation; either version 2 of the License, or (at your option) "
"any later version."
msgstr ""

#: xfce4-about/main.c:371
msgid ""
"The packages thunar, xfce4-appfinder, xfce4-panel, xfce4-session, "
"xfce4-settings, xfconf, xfdesktop and xfwm4 are distributed under the terms "
"of the GNU General Public License as published by the Free Software "
"Foundation; either version 2 of the License, or (at your option) any later "
"version."
msgstr ""

#: xfce4-about/main.c:517
#, c-format
msgid "Type '%s --help' for usage information."
msgstr "Escribi '%s --help' pa la información d'usu."

#: xfce4-about/main.c:523
msgid "Unable to initialize GTK+."
msgstr "Imposible aniciar GTK+."

#: xfce4-about/main.c:532
msgid "The Xfce development team. All rights reserved."
msgstr "L'equipu de desendolcu de XFCE. Tolos derechos acutaos."

#: xfce4-about/main.c:533
#, c-format
msgid "Please report bugs to <%s>."
msgstr "Informa de fallos en <%s>."

#: xfce4-about/main.c:543
msgid "Failed to load interface"
msgstr "Fallu al cargar la interfaz"

#. { N_("Project Lead"),
#. xfce_contributors_lead
#. },
#: xfce4-about/contributors.h:144
msgid "Core developers"
msgstr "Desendolcadores principales"

#: xfce4-about/contributors.h:147
msgid "Active contributors"
msgstr "Collaboradores activos"

#: xfce4-about/contributors.h:150
msgid "Servers maintained by"
msgstr "Sirvidores calteníos por"

#: xfce4-about/contributors.h:153
msgid "Translations supervision"
msgstr "Supervisión de les traducciones"

#: xfce4-about/contributors.h:156
msgid "Documentation supervision"
msgstr "Supervisión de la documentación"

#: xfce4-about/contributors.h:159
msgid "Translators"
msgstr "Traductores"

#: xfce4-about/contributors.h:162
msgid "Previous contributors"
msgstr "Collaboradores anteriores"

#: xfce4-about/xfce4-about.desktop.in:11
msgid "About Xfce"
msgstr "Tocante a XFCE"

#: xfce4-about/xfce4-about.desktop.in:12
msgid "Information about the Xfce Desktop Environment"
msgstr "Información tocante al entornu d'escritoriu Xfce"

#: xfce4-about/xfce4-about-dialog.glade:116
msgid "License"
msgstr "Llicencia"

#: xfce4-about/xfce4-about-dialog.glade:340
msgid "About the Xfce Desktop Environment"
msgstr "Tocante al entornu d'escritoriu XFCE"

#: xfce4-about/xfce4-about-dialog.glade:358
msgid "_Help"
msgstr "A_yuda"

#: xfce4-about/xfce4-about-dialog.glade:374
msgid "_Close"
msgstr "_Zarrar"

#: xfce4-about/xfce4-about-dialog.glade:437
msgid "Device"
msgstr "Preséu"

#: xfce4-about/xfce4-about-dialog.glade:449
#: xfce4-about/xfce4-about-dialog.glade:474
#: xfce4-about/xfce4-about-dialog.glade:499
#: xfce4-about/xfce4-about-dialog.glade:524
#: xfce4-about/xfce4-about-dialog.glade:551
#: xfce4-about/xfce4-about-dialog.glade:576
#: xfce4-about/xfce4-about-dialog.glade:601
#: xfce4-about/xfce4-about-dialog.glade:628
#: xfce4-about/xfce4-about-dialog.glade:653
#: xfce4-about/xfce4-about-dialog.glade:678
msgid "label"
msgstr "etiqueta"

#: xfce4-about/xfce4-about-dialog.glade:462
msgid "OS Name"
msgstr "Nome del SO"

#: xfce4-about/xfce4-about-dialog.glade:487
msgid "OS Type"
msgstr "Tipu de SO"

#: xfce4-about/xfce4-about-dialog.glade:512
msgid "Distributor"
msgstr "Distribuyidor"

#: xfce4-about/xfce4-about-dialog.glade:538
msgid "Xfce Version"
msgstr "Versión de XFCE"

#: xfce4-about/xfce4-about-dialog.glade:564
msgid "GTK Version"
msgstr "Versión de GTK"

#: xfce4-about/xfce4-about-dialog.glade:589
msgid "Kernel Version"
msgstr "Versión del kernel"

#: xfce4-about/xfce4-about-dialog.glade:615
msgid "CPU"
msgstr "CPU"

#: xfce4-about/xfce4-about-dialog.glade:641
msgid "Memory"
msgstr "Memoria"

#: xfce4-about/xfce4-about-dialog.glade:666
msgid "GPU"
msgstr "GPU"

#: xfce4-about/xfce4-about-dialog.glade:699
msgid "System"
msgstr "Sistema"

#: xfce4-about/xfce4-about-dialog.glade:741
msgid ""
"Xfce is a collection of programs that together provide a full-featured desktop environment.\n"
"The following programs are part of the Xfce core:"
msgstr ""

#: xfce4-about/xfce4-about-dialog.glade:779
msgid ""
"Xfce is also a development platform providing several libraries that help programmers create applications that fit in well with the desktop environment.\n"
"\n"
"Xfce components are licensed under free or open source licences; GPL or BSDL for applications and LGPL or BSDL for libraries. Look at the documentation, the source code or the Xfce website (<a href=\"https://www.xfce.org\">https://www.xfce.org</a>) for more information.\n"
"\n"
"Thank you for your interest in Xfce.\n"
"\n"
"\t- The Xfce Development Team"
msgstr ""

#: xfce4-about/xfce4-about-dialog.glade:809
msgid "About"
msgstr "Tocante a"

#: xfce4-about/xfce4-about-dialog.glade:843
msgid "Credits"
msgstr "Creitos"

#: xfce4-about/xfce4-about-dialog.glade:922
msgid "GPL"
msgstr "GPL"

#: xfce4-about/xfce4-about-dialog.glade:935
msgid "LGPL"
msgstr "LGPL"

#: xfce4-about/xfce4-about-dialog.glade:948
msgid "BSD"
msgstr "BSD"

#: xfce4-about/xfce4-about-dialog.glade:978
msgid "Copyright"
msgstr "Copyright"

#: xfce4-about/system-info.c:533 xfce4-about/system-info.c:555
#: xfce4-about/system-info.c:673 xfce4-about/system-info.c:721
msgid "Unknown"
msgstr "Desconocíu"

#. translators: This is the type of architecture for the OS
#: xfce4-about/system-info.c:658
msgid "64-bit (32-bit userspace)"
msgstr ""

#. translators: This is the type of architecture for the OS
#: xfce4-about/system-info.c:661
msgid "64-bit"
msgstr "64 bits"

#: xfce4-about/system-info.c:661
msgid "32-bit"
msgstr "32 bits"

#. translators: This is the name of the OS, followed by the build ID, for
#. * example:
#. * "Fedora 25 (Workstation Edition); Build ID: xyz" or
#. * "Ubuntu 16.04 LTS; Build ID: jki"
#: xfce4-about/system-info.c:729
#, c-format
msgid "%s; Build ID: %s"
msgstr ""

#: tests/test-ui.c:111
#, c-format
msgid ""
"A file named \"%s\" already exists in this directory, the file hasn't been "
"added."
msgstr "Nun s'amestó'l ficheru «%s» porque yá hai otru col mesmu nome."

#: tests/test-ui.c:122
msgid "Failed to migrate the old panel configuration"
msgstr "Fallu al migrar el panel de configuración vieyu"

#: tests/test-ui.c:172 tests/test-ui.c:197
msgid "Customize settings stored by Xfconf"
msgstr "Personaliza los axustes atroxaos por Xfconf"
